﻿using Microsoft.Extensions.DependencyInjection;
using SpeedIM.FrameWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace speed_im_sample
{
    class Startup : IStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.Inject("http://demo.speedim.cn:24087", "21012919420", "2affed91f45d43deaa94c6c25c90449e6c55e62e9af64a42a7ce880d0c22c4be");
            services.Inject<SpeedIM.Template.SignalR.IM>();
        }
    }
}

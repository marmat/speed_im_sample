﻿using SpeedIM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace speed_im_sample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string userID = "payne"; // 登录用户的id
            IMApp.Login(userID, result =>
            {
                IMApp.Show();//登录后打开聊天页面。
                IMApp.Extension.EnabledNotification();//启用通知栏消息
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IMApp.Show();  //打开聊天页面。
        }
    }
}
